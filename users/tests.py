from django.db.utils import IntegrityError
from django.test import TestCase

from users.models import User


class UserTestCase(TestCase):
    def test_user_0(self):
        self.assertEqual(User.objects.count(), 0)

    def test_user_create(self):
        User.objects.create_user("tst@mail.ru", "1234")
        self.assertEqual(User.objects.last().email, "tst@mail.ru")

    def test_user_create_same(self):
        User.objects.create_user("tst@mail.ru", "1234")
        self.assertRaises(IntegrityError, User.objects.create_user,
                          "tst@mail.ru", "5678")

    def test_user_default_type(self):
        User.objects.create_user("test@mail.ru", "1234")
        self.assertEqual(User.objects.last().user_type, 1)
