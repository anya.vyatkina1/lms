from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin

from django.utils import timezone
from django.utils.crypto import get_random_string

from .managers import CustomUserManager


# Create your models here.
class User(AbstractBaseUser, PermissionsMixin):
    USER_TYPE_CHOICES = (
        (1, 'Student'),
        (2, 'Teacher'),
        (3, 'Manager')
    )

    STUDENT_DEGREE_CHOICES = (
        (1, 'Bachelor'),
        (2, 'Specialist'),
        (3, 'Master'),
    )

    STUDENT_FORM_CHOICES = (
        (1, 'Full time'),
        (2, 'Part time'),
    )

    STUDENT_BASIS_CHOICES = (
        (1, 'Budget'),
        (2, 'Contractual')
    )

    username = None
    email = models.EmailField(unique=True, blank=True, verbose_name='email')
    password = models.CharField(max_length=128, blank=True,
                                verbose_name='password')

    reg_token = models.CharField(max_length=32, unique=True, null=False,
                                 blank=False, default=get_random_string)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)

    full_name = models.TextField(max_length=60, verbose_name='Full name')
    user_type = models.PositiveIntegerField(choices=USER_TYPE_CHOICES,
                                            default=1,
                                            verbose_name='User type')

    group = models.ForeignKey('study_course.Group', blank=True, null=True,
                              on_delete=models.CASCADE,
                              verbose_name='Group')

    date_joined = models.DateTimeField(default=timezone.now,
                                       verbose_name='Join date')
    degree = models.PositiveIntegerField(choices=STUDENT_DEGREE_CHOICES,
                                         blank=True, null=True,
                                         verbose_name='Degree')
    educ_form = models.PositiveIntegerField(choices=STUDENT_FORM_CHOICES,
                                            blank=True, null=True,
                                            verbose_name='Form')
    educ_basis = models.PositiveIntegerField(choices=STUDENT_BASIS_CHOICES,
                                             blank=True, null=True,
                                             verbose_name='Basis')

    city = models.TextField(max_length=60, verbose_name='City',blank=True, null=True)
    vk_link = models.URLField(blank=True, null=True, verbose_name='VK profile')
    inst_link = models.URLField(blank=True, null=True,
                                verbose_name='Instagram profile')
    linkedin_link = models.URLField(blank=True, null=True,
                                    verbose_name='Linkedin profile')
    fb_link = models.URLField(blank=True, null=True,
                              verbose_name='Facebook profile')

    objects = CustomUserManager()

    def __str__(self):
        return self.full_name + ' ' + self.email
