from django.apps import AppConfig


class UsersConfig(AppConfig):
    """
        Class with User module config
    """
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'users'
