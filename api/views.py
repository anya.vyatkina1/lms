from django.contrib.auth import login, logout
from django.http import Http404

from rest_framework import status
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from study_course.models import Course
from users.models import User
from . import serializers


def get_user_serializer_class(access_lvl='basic'):
    """
        Get user serializer based on access conditions
    :return: serializers.Serializer child
    """
    user_serializers = {
        'basic': serializers.UserSerializerBasic,
        'full': serializers.UserSerializerFull
    }
    return user_serializers[access_lvl]


def get_access_level(user):
    """
    :param user: instance of users.User
    :return: Return string with user access level
    """
    if user.is_staff:
        return 'full'
    return 'basic'


# Create your views here.
class UserList(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (SessionAuthentication,)

    def get(self, request):
        users_queryset = User.objects.filter(is_active=True)

        access_lvl = get_access_level(request.user)
        serializer_class = get_user_serializer_class(access_lvl)

        serializer = serializer_class(users_queryset, many=True)
        return Response({"users": serializer.data})


class GroupList(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (SessionAuthentication,)

    def get(self, request):
        print(request.user)
        need_group = request.user.group

        group_queryset = User.objects.filter(group=need_group)
        serializer_class = get_user_serializer_class()
        serializer = serializer_class(group_queryset, many=True)

        return Response({'users': serializer.data})


class CourseList(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (SessionAuthentication,)

    def get(self, request):
        if request.user.user_type == 2:
            course_queryset = Course.objects.filter(teacher=request.user)
        else:
            need_group = request.user.group
            course_queryset = Course.objects.filter(groups=need_group)

        serializer_class = serializers.CourseSerializer
        serializer = serializer_class(course_queryset, many=True)
        return Response({'courses': serializer.data})


class UserDetail(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (SessionAuthentication,)

    def get(self, request, **kwargs):
        try:
            user = User.objects.get(pk=kwargs['pk'])
        except User.DoesNotExist:
            return Response(Http404)

        access_lvl = get_access_level(request.user)
        serializer_class = get_user_serializer_class(access_lvl)
        serializer = serializer_class(user)
        return Response(serializer.data)


class CourseDetail(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (SessionAuthentication,)

    def get(self, request, **kwargs):
        try:
            course = Course.objects.get(id=kwargs['id'])
        except Course.DoesNotExist:
            return Response(Http404)

        serializer_class = serializers.CourseSerializer
        serializer = serializer_class(course)
        return Response(serializer.data)


class RegisterView(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        serializer = serializers.RegisterSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        user = serializer.validated_data['user']

        user.email = serializer.initial_data['email']
        user.set_password(serializer.initial_data['password'])
        user.is_active = True
        user.save()

        return Response(status=status.HTTP_200_OK)


class LoginView(APIView):
    permission_classes = (AllowAny,)
    authentication_classes = (SessionAuthentication,)

    def post(self, request):
        serializer = serializers.LoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        login(request, user)

        user_serializer = get_user_serializer_class()
        return Response(user_serializer(user).data)


class LogoutView(APIView):
    def post(self, request):
        logout(request)
        return Response()
