from rest_framework import serializers
from django.contrib.auth import authenticate, password_validation

from users.models import User
from study_course.models import Course, Group


class TeacherSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['full_name', 'email']


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ['name', 'faculty']


class UserSerializerFull(serializers.ModelSerializer):
    group = GroupSerializer(read_only=True, many=False)

    class Meta:
        model = User
        fields = ['full_name', 'email', 'group', 'degree', 'educ_form',
                  'educ_basis', 'city', 'vk_link', 'fb_link', 'inst_link',
                  'linkedin_link']


class UserSerializerBasic(serializers.ModelSerializer):
    group = GroupSerializer(read_only=True, many=False)

    class Meta:
        model = User
        fields = ['full_name', 'email', 'group', 'degree', 'educ_form', 'city',
                  'vk_link', 'fb_link', 'inst_link',
                  'linkedin_link']


class CourseSerializer(serializers.ModelSerializer):
    teacher = TeacherSerializer(read_only=True, many=False)
    groups = GroupSerializer(read_only=True, many=True)

    # trusted = TrustedSerializer(read_only=True, many=True)
    # material = MaterialSerializer(read_only=True, many=True)
    # homework = HomeworkSerializer(read_only=True, many=True)
    class Meta:
        model = Course
        fields = ['course_name', 'description', 'teacher', 'groups']


class RegisterSerializer(serializers.Serializer):
    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass

    reg_token = serializers.CharField()
    email = serializers.EmailField()
    password = serializers.CharField()

    def validate(self, attrs):
        try:
            user = User.objects.get(reg_token=attrs['reg_token'])
        except User.DoesNotExist as not_exist:
            raise serializers.ValidationError(
                "Incorrect reg_token") from not_exist

        password_validation.validate_password(password=attrs['password'])

        return {'user': user}


class LoginSerializer(serializers.Serializer):
    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass

    email = serializers.EmailField()
    password = serializers.CharField()

    def validate(self, attrs):
        user = authenticate(username=attrs['email'],
                            password=attrs['password'])

        if not user:
            raise serializers.ValidationError("Incorrect email or password")

        if not user.is_active:
            raise serializers.ValidationError("User is disabled")

        return {'user': user}
