from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from . import views

urlpatterns = [
    path('users/', views.UserList.as_view()),
    path('users/<int:pk>/', views.UserDetail.as_view()),
    path('classmates/', views.GroupList.as_view()),
    path('courses/', views.CourseList.as_view()),
    path('courses/<int:id>/', views.CourseDetail.as_view()),
    path(r'login/', views.LoginView.as_view()),
    path(r'logout/', views.LogoutView.as_view()),
    path(r'register/', views.RegisterView.as_view())
]

urlpatterns = format_suffix_patterns(urlpatterns)
