from django.db import models

from users.models import User


# Create your models here.
class Group(models.Model):
    name = models.CharField(max_length=50, verbose_name='Group name')
    faculty = models.CharField(max_length=100, verbose_name='Group faculty')
    grade = models.PositiveIntegerField(verbose_name='Group grade')

    def __str__(self):
        return self.name


class Course(models.Model):
    course_name = models.CharField(max_length=50, verbose_name='Course name')
    description = models.CharField(max_length=100,
                                   verbose_name='Course description')

    teacher = models.ForeignKey(User, on_delete=models.CASCADE,
                                verbose_name='Course teacher')

    groups = models.ManyToManyField(Group, verbose_name='Groups in course')

    def __str__(self):
        return self.course_name
