from django.contrib import admin

from study_course.models import Group, Course

# Register your models here.
admin.site.register(Group)
admin.site.register(Course)
